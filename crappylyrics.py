import requests
from html import unescape
import json


artist = input('Artist: ')
song = input('Song name: ')

print('-- Collecting lyrics...')
url = 'http://lyric-api.herokuapp.com/api/find/' + artist + '/' + song
r = requests.get(url)
data = r.json()
lyrics = unescape(data['lyric'])
print('-- Lyrics succesfully collected.')


fromlanguage = 'auto'
tolanguage = input('To language: ')


url = 'https://translate.googleapis.com/translate_a/single?client=gtx&sl={}&tl={}&dt=t&q={}'.format(fromlanguage, tolanguage, unescape(lyrics))
r = requests.get(url)
print('-- Lyrics succesfully translated.')
print('-- Parsing lyrics...')
data = json.loads(r.text)[0]
lyrics = ''
for line in data:
	lyrics += line[0]

print('\n\n' + lyrics)