# CrappyLyrics

## Description
CrappyLyrics is a Python script that allows you to easy download lyrics and translate them using Google Translate.

## Website
I also made this script into a Django website. You can visit it at https://samvanderkris.xyz/translyric.

## Requirements
- Python 3
- Requests (Python library)
- A computer

## Installation
- Clone this repository
- Run crappylyrics.py in your terminal
- You're done

## Licensing stuff
This project is licensed under the MIT license. You can do pretty much everything you want with it. You can edit and/or distribute it in any way you like, as long as you don't hold me liable for anything. Also, please note that the API I used is meant for the Google Translate extension for Chrome and is not officially supported by Google. The lyrics API is made by [rhnvrm](https://github.com/rhnvrm/lyric-api) and uses lyrics.wikia.com as source.